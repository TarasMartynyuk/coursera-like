import React from 'react'
import './page404.css'

const Page404 = () => {
    return (
        <div className="container">
            <div className="h3 m-3"> Не знайдено!</div>
            <hr />
            <div className="h6 m-3">Помилка 404</div>
        </div>
    )
}

export default Page404