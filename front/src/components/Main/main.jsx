import React, {useState, useEffect} from 'react'
import Footer from '../Footer/footer'
import axios from 'axios'
import './main.css'
import mainImg1 from "../../assets/main_page_img1.png"
import mainImg2 from "../../assets/main_page_img2.png"
import courseImg from '../../assets/course_img.png'
import card1Img from '../../assets/card_1.png'
import card2Img from '../../assets/card_2.png'
import card3Img from '../../assets/card_3.png'
import person1Img from '../../assets/person1.png'
import person2Img from '../../assets/person2.png'
import person3Img from '../../assets/person3.png'
import { Link } from 'react-router-dom'
 

const Main = ({user, apiUrl}) => {

    const [courses, setCourses] = useState()


    useEffect(() => {
        let mounted = true

        axios.get(apiUrl)
          .then(res => {
                const coursesFirst = res.data.slice(0, 3)
                mounted && setCourses([...coursesFirst])   
            })
            .catch(err => console.error(err))
    
        return () => mounted = false
    }, [])


    const coursesList = courses && courses.map(el => {
        return <div key={el.id} className="col-md-4  p-2 p-md-3">
            <Link to={{pathname: `/courses/${el.id}`, state: user}}>
                <div className="card course">
                    <img className="card-img-top course" src={courseImg} alt={'courseImg'} />
                    <div className="card-body course">
                        <h4 className="card-title course">{el.name}</h4>
                    </div>
                </div>
            </Link>
        </div>
    }
    )

    return (
        <>
            <div className="container">
                <div className="ml-5 mt-5">
                    <div className="ellipse"></div>
                    <div className="ellipse"></div>
                    <div className="ellipse"></div>
                </div>
                <div className="row mb-5 mt-2">
                    <div className="col-md-6">
                        <img className="main-img" src={mainImg1} alt={'courseImg'} />
                        <div className="ml-5">
                            <div className="column"></div>
                            <div className="column"></div>
                            <div className="column"></div>
                        </div>
                    </div>
                
                    <div className="col-md-6">
                        <p className="quote-text mb-5">Знання варто добувати та поширювати</p>
                        <Link className="register-link" to={{pathname: "/register", state: user}}><button type="button" className="btn main-btn btn-primary btn-lg btn-block mt-5">Зареєструватися</button></Link>
                    </div>
                    
                    <div className="col-12 pt-3">

                    </div>
                </div>
            
                <div className="container our-courses-list mt-5 mb-5 mt-md-5 mb-md-5 py-5">
                    <h2 className="main px-auto">Наші курси</h2>
                    <div className="row d-flex justify-content-start mx-4">
                        {coursesList}
                    </div>
                    <div className="main-all text-center mt-3">
                        <Link to={{pathname: "/courses", state: user}}>Переглянути всі</Link>
                    </div>
                </div>

                <div className="container my-5 pt-4 pb-3">
                    <div className="card-group">
                        <div className="card card-without-border">
                            <img className="card-img-top card-img" src={card1Img} alt={'Будь коли'}/>
                            <div className="card-body text-center">
                                <h5 className="card-title">БУДЬ-КОЛИ</h5>
                                <p className="card-text">Навчайтесь у зручний для вас час - всi матерiали платформи доступнi цiлодобово.</p>
                            </div>
                        </div>
                        <div className="card card-without-border">
                            <img className="card-img-top card-img" src={card2Img} alt={'Будь-де'}/>
                            <div className="card-body text-center">
                                <h5 className="card-title">БУДЬ-ДЕ</h5>
                                <p className="card-text">Для навчання вам потрiбен виключно доступ до Iнтернету та бажання вчитись!</p>
                            </div>
                        </div>
                        <div className="card card-without-border">
                            <img className="card-img-top card-img" src={card3Img} alt={'Будь-що'}/>
                            <div className="card-body text-center">
                            <h5 className="card-title">БУДЬ-ЩО</h5>
                            <p className="card-text">Великий вибiр матерiалiв на рiзнi теми та сфери iнтересiв.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row my-5 pb-5">
                    <div className="col-md-6">
                        <img className="main-img" src={mainImg2} alt={'courseImg'} />
                    </div>
                    <div className="col-md-6">
                        <p className="quote-text">Зробіть наступний крок на шляху до своїх знань</p>
                        <p>Охоплюючи велику кількість тем, ми пропонуємо широкий спектр навчальних можливостей - від практичних проектів та курсів до готових до роботи сертифікатів та навчальних програм.</p>
                        <Link className="register-link" to={{pathname: "/register", state: user}}><button type="button" className="btn main-btn btn-primary btn-lg btn-block">Зареєструватися</button></Link>
                    </div>
                </div>

                <div className="pl-3 ml-5 mt-5 pt-5 row">
                        <div className="col-2">
                            <div className="ellipse21">

                            </div>
                        </div>
                        <div className="col-2">
                            <div className="ellipse22">

                            </div>
                        </div>
                    </div>

                <div className="container my-5">
                    <h2 className="text-center mb-4">Відгуки</h2>
                    <div className="card-group">
                        <div className="card card-round-border mx-1 px-1 mx-lg-5 px-lg-4">
                            <img className="card-img-top card-img" src={person1Img} alt={'Олексій'}/>
                            <div className="card-body text-center">
                                <h5 className="card-title">Олексій, 23р.</h5>
                                <div className="card-underline"></div>
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div className="card card-round-border mx-1 px-1 mx-lg-5 px-lg-4">
                            <img className="card-img-top card-img" src={person3Img} alt={'Маргарита'}/>
                            <div className="card-body text-center">
                                <h5 className="card-title">Маргарита, 20р</h5>
                                <div className="card-underline"></div>
                                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div className="card card-round-border mx-1 px-1 mx-lg-5 px-lg-4">
                            <img className="card-img-top card-img" src={person2Img} alt={'Кирило'}/>
                            <div className="card-body text-center">
                            <h5 className="card-title">Кирило, 21р</h5>
                            <div className="card-underline"></div>
                            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer user={user}/>
        </>
    )
}

export default Main