import React from 'react'
import './footer.css'
import { Link } from 'react-router-dom'


const Footer = ({user}) => {
    return (
        <div className="footer-basic">
            <footer>
                <ul className="list-inline my-2">
                    <li className="list-inline-item"><Link to={{pathname: "/", state: user}}>Головна</Link></li>
                    <li className="list-inline-item"><Link to={{pathname: "/about", state: user}}>Про нас</Link></li>
                    <li className="list-inline-item"><a target="_blank" href={ "https://www.instagram.com/ansttss/"}>Підтримка</a></li>
                </ul>
                <p className="footer-quote my-3">
                    “Багато чого не зробиш, поки не вивчишся. Але багато треба зробити, щоб навчитись.”
                </p>
                <p className="copyright">ZnaTy© | All Rights Reserved</p>
            </footer>
        </div>
    )
}

export default Footer