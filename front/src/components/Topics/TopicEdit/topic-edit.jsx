import React, {useState, useEffect} from 'react'
import './topic-edit.css'
import axios from 'axios'
import { useForm } from "react-hook-form"
import { NavLink, useHistory, useLocation,  useParams } from 'react-router-dom'
import icon from "../../../assets/course_icon.png"
import {Accordion, Card} from  'react-bootstrap'

const TopicEdit = ({isMobile, apiUrl, user}) => {


    const [wrongData, setWrongData] = useState(false)
    const [topic, setTopic] = useState()
    const { register, handleSubmit, errors } = useForm()
  
    let history = useHistory()
    let location = useLocation()
    let { courseId, topicId } = useParams()
  

   useEffect(() => {
    let mounted = true

    axios.get(`${apiUrl}/get_course?course_id=${courseId}`)
      .then(res => {
         if (mounted){
            const topic = res.data.topics.filter(el => el.topic_id === parseInt(topicId))
            setTopic(...topic)
         } 
      })
      .catch(err => console.error(err))

    return () => mounted = false
  }, [courseId, topicId])


   if (!topic){
      return (
          <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
          <h5 >
              Завантаження...
          </h5>
      </div>
      )
  }

    const onSubmit = data => {

  
        axios.post(apiUrl+'/edit_topic', 
            {
                "topic_id": parseInt(topicId),
                "title": data.title,
                "body": data.body,
                "homework_description": data.hw_descr,
                "homework_deadline" : "2021-04-10 09:00"
            },
            {
              headers: {
                  'content-type': 'application/json',
                  'Authorization': `Basic ${location.state.sessionId}`
              }  
            }).then(res => {
                setWrongData(false)
              history.push({
                  pathname: `/courses/${courseId}/edit`,
                  state: user
                })
      
            })
            .catch(err => {
                setWrongData(true)
                console.log('Error on course creating', err);
            })
    }
  
      return (
        <div className="container mt-5">
        <div className="row">
             <div className="col-12 auth mx-auto">
                
                        <form onSubmit={handleSubmit(onSubmit)}>
                
                            <div className="mb-3 col">
                                <label className="col-form-label px-3">Редагування</label>
                                <div className="col-12">
                                    <input name="title" defaultValue={topic.title}
                                    ref={register({
                                        required: { value: true, message: "Введіть назву!" }, minLength: { value: 6, message: "Мінімальна довжина  6 символів!" }
                                    })} 
                                    className={`form-control ${errors.title? 'is-invalid' : ''}`} />
                                    {errors.title && <p className="text-danger">{errors.title.message}</p>}
                                </div>
                            </div>
                            <div className="mb-3 col">
                                <div className="col-12 ">
                                    <textarea name="body"  style={{height: 300}} type="body" ref={register({
                                        required: { value: true, message: "Введіть опис!" },
                                        minLength: { value: 10, message: "Мінімальна довжина 10 символів!" }
                                    })} className={`form-control ${errors.body ? 'is-invalid' : ''}`} 
                                    defaultValue={topic.body}
                                    />
                                    {errors.body && <p className="text-danger">{errors.body.message}</p>}
                                </div>
                            </div>

                            <div className="mb-3 col">
                                <label className="col-form-label px-3">Домашнє завдання</label>
                                <div className="col-12 ">
                                    <textarea name="hw_descr"  style={{height: 100}} type="body" ref={register({
                                        required: { value: true, message: "Введіть опис!" },
                                        minLength: { value: 10, message: "Мінімальна довжина 10 символів!" }
                                    })} className={`form-control ${errors.hw_descr ? 'is-invalid' : ''}`} 
                                    defaultValue={topic.homework_description}
                                    />
                                    {errors.hw_descr && <p className="text-danger">{errors.hw_descr.message}</p>}
                                </div>
                            </div>        
                            <div className="mb-4 col authcol">
                                <button type="submit" className="btn btn-primary btn-block login-btn mb-3">Змінити</button>
                                {wrongData && <p className="text-danger">Щось пішло не так!</p>}
                            </div>
                        </form>
                    </div>
            </div>
    </div>
      )

}

export default TopicEdit