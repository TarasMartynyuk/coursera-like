import React, {useState} from 'react'
import axios from 'axios'
import { useForm } from "react-hook-form"
import { NavLink, useHistory, useLocation, useParams } from 'react-router-dom'
import './topic-create.css'

const TopicCreate = ({isMobile, apiUrl, user}) => {

  const [wrongData, setWrongData] = useState(false)
  const { register, handleSubmit, errors } = useForm()

  let history = useHistory()
  let location = useLocation()
  let { courseId } = useParams()


  const onSubmit = data => {

      axios.post(apiUrl+'/add_topic', 
          {
            "course_id": parseInt(courseId),
            "title": data.topic_name,
            "body": data.topic_material,
            "homework_description": data.hw,
            "homework_deadline" : "2021-04-09 12:00"
          },
          {
            headers: {
                'content-type': 'application/json',
                'Authorization': `Basic ${location.state.sessionId}`
            }  
          }
          )
          .then(res => {
              setWrongData(false)
             
              history.push({
                pathname: `/courses/${courseId}/edit`,
                state: user
              })
    
          })
          .catch(err => {
              setWrongData(true)
              console.log('Error on course creating', err);
          })
  }

    return (
      <div className="container">
      <div className="row">
           <div className="col-md-8 col-sm-12 auth mb-5 mx-auto">
               
                      <form onSubmit={handleSubmit(onSubmit)}>
                         
                          <div className="mb-3 col">
                              <label className="col-form-label px-3">Нова тема </label>
                              <div className="col-12">
                                  <input name="topic_name" placeholder="Назва теми..." ref={register({
                                      required: { value: true, message: "Введіть назву теми!" }, minLength: { value: 6, message: "Мінімальна довжина  6 символів!" }
                                  })} 
                                  className={`form-control ${errors.topic_name ? 'is-invalid' : ''}`} />
                                  {errors.topic_name && <p className="text-danger">{errors.topic_name.message}</p>}
                              </div>
                          </div>
                          <div className="mb-3 col">
                              <div className="col-12 ">
                                  <textarea name="topic_material" style={{height: 150}} placeholder="Матеріали до теми......" type="topic_material" ref={register({
                                      required: { value: true, message: "Введіть матеріали!" },
                                      minLength: { value: 10, message: "Мінімальна довжина 10 символів!" }
                                  })} className={`form-control ${errors.topic_material ? 'is-invalid' : ''}`} />
                                  {errors.topic_material && <p className="text-danger">{errors.topic_material.message}</p>}
                              </div>
                          </div>
                          <div className="mb-3 col">
                              <label className="col-form-label px-3">Додаткові файли</label>
                              <div className="col-12 ">
                                  <input name="hw_work" disabled  placeholder="Перетягніть сюди файли..."

                                  className={`form-control ${errors.hw_work ? 'is-invalid' : ''}`} />
                                
                              </div>
                          </div>
                          <div className="mb-5 col">
                          <label className="col-form-label px-3">Домашнє завдання </label>
                          <div className="col-12 ">
                             <textarea name="hw" placeholder="Домашнє завдання очікуване від студентів..." ref={register({
                                      required: { value: true, message: "Введіть домашнє завдання!" }, minLength: { value: 10, message: "Мінімальна довжина  10 символів!" }
                                  })} 
                                  className={`form-control ${errors.hw ? 'is-invalid' : ''}`} />
                                  {errors.hw && <p className="text-danger">{errors.hw.message}</p>}
                                  </div>
                          </div>
                          <div className="mb-4 col authcol">
                              <button type="submit" className="btn btn-primary btn-block login-btn mb-3">Додати тему</button>
                              {wrongData && <p className="text-danger">Щось пішло не так!</p>}
                          </div>
                      </form>
                  </div>
              </div>
  </div>
    )
}

export default TopicCreate