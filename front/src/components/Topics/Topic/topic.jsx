import React, {useState, useEffect} from 'react'
import './topic.css'
import axios from 'axios'
import { useForm } from "react-hook-form"
import { NavLink,Link,  useHistory, useLocation,  useParams } from 'react-router-dom'
import icon from "../../../assets/course_icon.png"
import {Accordion, Card} from  'react-bootstrap'

const Topic = ({isMobile, apiUrl, user}) => {

    const [topic, setTopic] = useState()

    let history = useHistory()
    let location = useLocation()
    let { courseId, topicId } = useParams()
  


   useEffect(() => {
    let mounted = true

    axios.get(`${apiUrl}/get_course?course_id=${courseId}`)
      .then(res => {
         if (mounted){
            const topic = res.data.topics.filter(el => el.topic_id === parseInt(topicId))
            setTopic(...topic)
         } 
      })
      .catch(err => console.error(err))

    return () => mounted = false
  }, [courseId, topicId])

  

   if (!topic){
      return (
          <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
          <h5 >
              Завантаження...
          </h5>
      </div>
      )
  }




    return (
        <div className ="container my-3  topicPage">
               <div className="row "> 
                    <div className="col-12">
                    <h2 className="mt-3 mb-4">Тема: {topic.title}</h2>
                    </div> 
                    <div className="col-12">
                    <Link to={{pathname: `/courses/${courseId}/topic/${topicId}/edit`, state: location.state}}> 
                        <span className="mx-2 actions">Редагувати</span>
                    </Link> 
                        <span className="mx-2 actions">Видалити</span>
                    </div>
                    <div className="col-12">
                    <p className="topic_descr mt-3">
                      {topic.body}
                    </p>
                   </div>

                <div className="col-12">
                        <h2 className="mt-3 mb-4">Домашнє завдання </h2>
                        <p className="blockquote">{topic.homework_description}</p>
                </div> 
                </div>
         </div>
    )
}

export default Topic