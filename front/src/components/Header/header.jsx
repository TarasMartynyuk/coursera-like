import React, { useState } from 'react'
import { NavLink, Link, useHistory } from 'react-router-dom'
import { NavDropdown, Nav, Navbar } from 'react-bootstrap'
import './header.css'
import logo from '../../assets/logo.svg'


const Header = ({ isMobile, isAuth, user, handleUser }) => {

    let history = useHistory()

    const logOut = () => {
        handleUser(null)
        localStorage.removeItem("sessionId")
        history.push({
            pathname: '/',
            state: null,
        })
    }

    const closeDropdownMenu = () => window.document.getElementById('nav-dropdown').click()

    return (
        <>
            <Navbar bg="primary" expand={`${isAuth ? 'md' : ''}`} className="navbar-dark navheader">
                <Link className={`navbar-brand ${!isAuth ? 'mx-auto' : 'ml-4'}`} to={{pathname: '/', state: user}}>
                    <img src={logo} alt="logo" width="30" height="60" className="d-inline-block align-top" />
                </Link>
                {isAuth && <Navbar.Toggle aria-controls="basic-navbar-nav" />}
                <Navbar.Collapse id="basic-navbar-nav">
                    {isAuth && <>
                        <Nav className="ml-auto">
                            <NavLink className="nav-item nav-link mx-3 navheader" activeClassName="active" to={{pathname: '/courses', state: user}}>Курси</NavLink>
                            <NavLink className="nav-item nav-link mx-3 navheader" activeClassName="active" to={{pathname: '/about', state: user}}>Про нас</NavLink>
                            {!user ?
                                <>
                                    <NavLink className="nav-item nav-link mx-3 navheader" activeClassName="active" to={{pathname: '/login', state: user}}>Увійти</NavLink>
                                    <NavLink className="nav-item nav-link mx-3 navheader" activeClassName="active" to={{pathname: '/register', state: user}}>Зареєструватися</NavLink>
                                </>
                                :
                                <NavDropdown alignRight className={`${isMobile ? 'drop-sm' : ''} mx-3 navheader`} title={`Вітаємо, ${user && user.name}!`} id="nav-dropdown" >
                                    <Link className="dropdown-item" onClick={closeDropdownMenu} to={{pathname: '/personal', state: user}}>Особиста інформація</Link>
                                    {user && user.role === 'teacher'
                                        ? <Link className="dropdown-item" onClick={closeDropdownMenu} to={{pathname: '/homework', state: user}}>Запити дз</Link>
                                        : <Link className="dropdown-item" onClick={closeDropdownMenu} to={{pathname: '/marks', state: user}}>Оцінки</Link>}
                                    <Link className="dropdown-item" onClick={closeDropdownMenu} to={{pathname: '/mycourses', state: user}}>Мої курси</Link>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item onClick={logOut}>Вийти</NavDropdown.Item>
                                </NavDropdown>}
                        </Nav></>}
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default Header