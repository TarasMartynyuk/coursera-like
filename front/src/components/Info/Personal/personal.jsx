import React from "react";
import { useLocation } from "react-router-dom";
import "./personal.css";

const Personal = () => {
    let location = useLocation();

    return (
        <div className="container mt-3">
            <div className="text-center">
                <svg
                    width="200"
                    height="200"
                    viewBox="0 0 200 200"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <circle cx="100" cy="100" r="99.5" stroke="black" />
                    <path d="M95.84 57V141H102.44V57H95.84Z" fill="black" />
                </svg>
            </div>

            <div className="text-center display-4 my-2">
                <span>{location.state.name} </span>
                <span>{location.state.surname}</span>
            </div>

            <h5 className="mt-5 mb-4">Основна інформація:</h5>

            <div className="border">
                <div className="py-4 px-4">
                    <div className="form-group">
                        <span className="mr-2 font-weight-bold">
                            Ім'я: 
                        </span>
                        <span> {location.state.name}</span>
                    </div>
                    <div className="form-group">
                        <span className="mr-2 font-weight-bold">
                            Прізвище: 
                        </span>
                        <span> {location.state.surname}</span>
                    </div>
                    <div className="form-group">
                        <span className="mr-2 font-weight-bold">
                            Роль: 
                        </span>
                        <span> {location.state && location.state.role==='student' ? 'Студент' : 'Викладач'}</span>
                    </div>
                    <div className="form-group">
                        <span className="mr-2 font-weight-bold">
                            Пошта: 
                        </span>
                        <span> {location.state.email}</span>
                    </div>
                    <div className="form-group">
                        <span className="mr-2 font-weight-bold">
                            Мова: 
                        </span>
                        <span>Українська</span>
                    </div>
                    
                </div>
            </div>
        </div>
    );
};

export default Personal;
