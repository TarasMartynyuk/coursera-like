import React, { useState, useEffect } from 'react'
import {Accordion, Card} from  'react-bootstrap'
import { Link, useLocation } from 'react-router-dom'
import axios from 'axios'
import './mark.css'

const Mark = ({apiUrl}) => {

    const [courses, setCourses] = useState()

    let location = useLocation()

    useEffect(() => {
        let mounted = true
          axios.get(`${apiUrl}/get_enrolled_in_courses`,{
              headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Basic ${location.state.sessionId}`
            }
          })
          .then(res => mounted && setCourses([...res.data]))
          .catch(err => console.error(err))
        
        
        return () => mounted = false
    }, [])



    const marks = courses && courses.map(el => {
      return <Card key={el.id}>
          <Accordion.Toggle as={Card.Header}  eventKey={el.id}>
            {el.name}
          </Accordion.Toggle>
          <Accordion.Collapse eventKey={el.id}>
            <Card.Body>
              <div className="marks-name py-2 px-1" >
                <span>Назва дз</span>
                <span>Оцінено (5/5)</span>
              </div>
              <div className="marks-name py-2 px-1">
                <span>Назва дз</span>
                <span>Оцінено (3/5)</span>
              </div>
            </Card.Body>
          </Accordion.Collapse>
      </Card>
    })


    if (!courses){
      return (
          <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
          <h5 >
              Завантаження...
          </h5>
      </div>
      )
    }

    return (
        <div className ="container mt-3 marks">
           <div className="text-center">
                <svg
                    width="200"
                    height="200"
                    viewBox="0 0 200 200"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <circle cx="100" cy="100" r="99.5" stroke="black" />
                    <path d="M95.84 57V141H102.44V57H95.84Z" fill="black" />
                </svg>
            </div>

            <div className="text-center display-4 my-2 ">
                <span>{location.state.name} </span>
                <span>{location.state.surname}</span>
            </div>
          <Accordion className="mt-5">
            {marks}
          </Accordion>
        </div>

    )
}

export default Mark