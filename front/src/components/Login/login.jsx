import React, { useState } from 'react'
import './login.css'
import axios from 'axios'
import { NavLink, useHistory, useLocation } from 'react-router-dom'
import { useForm } from "react-hook-form"


const Login = ({ handleEntered, sessionId, apiUrl }) => {

    const [wrongData, setWrongData] = useState(false)
    const { register, handleSubmit, errors } = useForm()

    let history = useHistory()

    const onSubmit = data => {
        // console.log("Login", data)
  

        axios.post(apiUrl, 
            {
                username: data.email,
                password: data.password
            })
            .then(res => {
                setWrongData(false)
                //console.log('Authenticated', res.data) 
                const resData = res.data
                const fullName = resData.fullName.split(" ")
                const user = { name: fullName[0], surname: fullName[1], email: data.email, role: resData.role, sessionId: resData.token}
                handleEntered(user)
                localStorage.setItem("sessionId", resData.token)
                history.push({
                    pathname: '/',
                    state: user
                  })
            })
            .catch(err => {
                setWrongData(true)
                console.log('Error on Authentication', err);
            })
    }





    return (
        <div className="container">
            <div className="row">
                <div className="col-md-8 col-sm-12 mx-auto">
                    <div className="card auth mb-5">
                        <div className="card-header auth">
                            <ul className="nav nav-tabs card-header-tabs">
                                <li className="nav-item login-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/register">Реєстрація</NavLink>
                                </li>
                                <li className="nav-item login-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/login">Логін</NavLink>
                                </li>
                            </ul>
                        </div>
                        <div className="card-body auth">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="my-3 col">
                                    <h4 className="px-3">Увійдіть у свій профіль</h4>
                                </div>
                                <div className="mb-3 col">
                                    <label className="col-form-label px-3">Пошта</label>
                                    <div className="col-12">
                                        <input name="email" ref={register({
                                            required: { value: true, message: "Введіть пошту!" }, maxLength: 30, pattern: {
                                                value: /\S+@\S+\.\S+/,
                                                message: "Введіть коректну пошту!"
                                            }
                                        })} className={`form-control ${errors.email ? 'is-invalid' : ''}`} />
                                        {errors.email && <p className="text-danger">{errors.email.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-5 col">
                                    <label className="col-form-label px-3">Пароль</label>
                                    <div className="col-12 ">
                                        <input name="password" type="password" ref={register({
                                            required: { value: true, message: "Введіть пароль!" },
                                            minLength: { value: 6, message: "Мінімальна довжина пароля 6 символів!" }, maxLength: 20
                                        })} className={`form-control ${errors.password ? 'is-invalid' : ''}`} />
                                        {errors.password && <p className="text-danger">{errors.password.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-4 col authcol">
                                    <button type="submit" className="btn btn-primary btn-block login-btn mb-3">Увійти</button>
                                    {wrongData && <p className="text-danger">Пароль або пошта неправильні!</p>}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login