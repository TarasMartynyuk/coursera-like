import React, {useState} from 'react'
import './register.css'
import axios from 'axios'
import { NavLink, useHistory } from 'react-router-dom'
import { useForm } from "react-hook-form"


const Register = ({apiUrl, handleEntered, sessionId }) => {

    const [wrongData, setWrongData] = useState(false)
    const { register, handleSubmit, errors } = useForm()

    let history = useHistory()


    const onSubmit = data => {
         //console.log("Register:", data)

          axios.post(apiUrl, 
            {
                username: data.email,
                password: data.password,
                full_name: `${data.firstName} ${data.secondName}`,
                role: data.role
            })
            .then(res => {
                setWrongData(false)
                //console.log('Register', res.data)
        
              history.push({
                    pathname: '/login',
                    state: null
                })
            })
            .catch(err => {
                setWrongData(true)
                console.log('Error on Authentication', err);
            })
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-8 col-sm-12 mx-auto">
                    <div className="card auth mb-5">
                        <div className="card-header auth">
                            <ul className="nav nav-tabs card-header-tabs">
                                <li className="nav-item login-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/register">Реєстрація</NavLink>
                                </li>
                                <li className="nav-item login-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/login">Логін</NavLink>
                                </li>
                            </ul>
                        </div>
                        <div className="card-body auth">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="mt-3 mb-4 col">
                                    <h4 className="px-3">Створіть новий профіль</h4>
                                </div>

                                <div className="mb-3 row d-flex justify-content-around">
                                    <div className="form-check my-3">
                                        <input className="form-check-input" ref={register} type="radio" name="role" value="student" defaultChecked />
                                        <label className="form-check-label">
                                            Я студент
                                    </label>
                                    </div>
                                    <div className="form-check my-3">
                                        <input className="form-check-input" ref={register} type="radio" name="role" value="teacher" />
                                        <label className="form-check-label" >
                                            Я викладач
                                    </label>
                                    </div>
                                </div>
                                <div className="mb-3 col">
                                    <label className="col-form-label px-3">Ім’я</label>
                                    <div className="col-12">
                                        <input type="text" name="firstName" ref={register({
                                            pattern: { value: /^[А-Яа-яЁёЇїІіЄєҐґ'A-Za-z]+$/i, message: "Тільки буквені символи!" },
                                            required: { value: true, message: "Введіть імя!" }, maxLength: 20
                                        })} className={`form-control ${errors.firstName ? 'is-invalid' : ''}`} />
                                        {errors.firstName && <p className="text-danger">{errors.firstName.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-3 col">
                                    <label className="col-form-label px-3">Прізвище</label>
                                    <div className="col-12">
                                        <input type="text" name="secondName" ref={register({
                                            pattern: { value: /^[А-Яа-яЁёЇїІіЄєҐґ'A-Za-z]+$/i, message: "Тільки буквені символи!" },
                                            required: { value: true, message: "Введіть прізвище!" }, maxLength: 20
                                        })} className={`form-control ${errors.secondName ? 'is-invalid' : ''}`} />
                                        {errors.secondName && <p className="text-danger">{errors.secondName.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-3 col">
                                    <label className="col-form-label px-3">Пошта</label>
                                    <div className="col-12">
                                        <input name="email" ref={register({
                                            required: { value: true, message: "Введіть пошту!" }, maxLength: 30, pattern: {
                                                value: /\S+@\S+\.\S+/,
                                                message: "Введіть коректну пошту!"
                                            }
                                        })} className={`form-control ${errors.email ? 'is-invalid' : ''}`} />
                                        {errors.email && <p className="text-danger">{errors.email.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-5 col">
                                    <label className="col-form-label px-3">Пароль</label>
                                    <div className="col-12 ">
                                        <input name="password" type="password" ref={register({
                                            required: { value: true, message: "Введіть пароль!" },
                                            minLength: { value: 6, message: "Мінімальна довжина пароля 6 символів!" }, maxLength: 20
                                        })} className={`form-control ${errors.password ? 'is-invalid' : ''}`} />
                                        {errors.password && <p className="text-danger">{errors.password.message}</p>}
                                    </div>
                                </div>
                                <div className="mb-4 col authcol">
                                    <button type="submit" className="btn btn-primary btn-block login-btn mb-3">Зареєструватися</button>
                                    {wrongData && <p className="text-danger">Щось пiшло не так!</p>}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register