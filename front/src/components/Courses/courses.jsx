import React, { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'
import axios from 'axios'
import './courses.css'
import courseImg from '../../assets/course_img.png'

const Courses = ({ isAll, user, apiUrl }) => {

    const [courses, setCourses] = useState()

    let location = useLocation()

    useEffect(() => {
        let mounted = true

        if(isAll){
            axios.get(`${apiUrl}/get_all_courses`)
            .then(res =>mounted && setCourses([...res.data]))
            .catch(err => console.error(err))
        }else{
            axios.get(`${apiUrl}/get_enrolled_in_courses`,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Basic ${location.state.sessionId}`
              }
            })
            .then(res =>mounted && setCourses([...res.data]))
            .catch(err => console.error(err))
        }
    
        return () => mounted = false
    }, [])


    const coursesList = courses && courses.map(el => {
        return <div key={el.id} className="col-xl-4 col-lg-4 col-md-6 col-sm-12  p-4 p-md-5">
            <Link to={{pathname: `/courses/${el.id}`, state: user}}>
                <div className="card course">
                    <img className="card-img-top" src={courseImg} alt={'course_img'} />
                    <div className="card-body">
                        <h4 className="card-title">{el.name}</h4>
                    </div>
                    <div className="rect"></div>
                </div>
            </Link>
        </div>
    }
    )

    if (!courses){
        return (
            <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
            <h5 >
                Завантаження...
            </h5>
        </div>
        )

    }


    return (
        <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
             {isAll ? <h1>Нашi курси</h1> : <h1>Мої курси</h1>}
             {!isAll && location.state && location.state.role==='teacher' &&
                <div className="text-center my-3">
                     <Link className="register-link" to={{pathname: "/create_course", state: user}}><button type="button" className="btn main-btn btn-primary btn-lg my-4">Створити новий курс</button></Link>
                </div>
             }
            <div className="row d-flex justify-content-start">
                {coursesList}
            </div>
        </div>
    )
}

export default Courses