import React, {useState} from 'react'
import './course-create.css'
import axios from 'axios'
import { useForm } from "react-hook-form"
import { NavLink, useHistory, useLocation } from 'react-router-dom'

const CourseCreate = ({isMobile, apiUrl, user}) => {

  const [wrongData, setWrongData] = useState(false)
  const { register, handleSubmit, errors } = useForm()

  let history = useHistory()
  let location = useLocation()

  const onSubmit = data => {

      axios.post(apiUrl+'/create_course', 
          {
            "name": data.course_name,
            "description": data.course_descr
          },
          {
            headers: {
                'content-type': 'application/json',
                'Authorization': `Basic ${location.state.sessionId}`
            }  
          }
          )
          .then(res => {
              setWrongData(false)
        
            history.push({
                pathname: `/courses/${res.data}/edit`,
                state: user
            })
    
          })
          .catch(err => {
              setWrongData(true)
              console.log('Error on course creating', err);
          })
  }

    return (
      <div className="container">
      <div className="row">
           <div className="col-md-8 col-sm-12 mb-5 auth mx-auto">
           
                      <form onSubmit={handleSubmit(onSubmit)}>
                         
                          <div className="mb-3 col">
                              <label className="col-form-label px-3">Назва курсу</label>
                              <div className="col-12">
                                  <input name="course_name" ref={register({
                                      required: { value: true, message: "Введіть назву!" }, minLength: { value: 6, message: "Мінімальна довжина  6 символів!" }
                                  })} 
                                  className={`form-control ${errors.course_name ? 'is-invalid' : ''}`} />
                                  {errors.course_name && <p className="text-danger">{errors.course_name.message}</p>}
                              </div>
                          </div>
                          <div className="mb-5 col">
                              <label className="col-form-label px-3">Опис курсу</label>
                              <div className="col-12 ">
                                  <textarea name="course_descr" type="course_descr" style={{height: 150}} ref={register({
                                      required: { value: true, message: "Введіть опис!" },
                                      minLength: { value: 10, message: "Мінімальна довжина 10 символів!" }
                                  })} className={`form-control ${errors.course_descr ? 'is-invalid' : ''}`} />
                                  {errors.course_descr && <p className="text-danger">{errors.course_descr.message}</p>}
                              </div>
                          </div>
                          <div className="mb-4 col authcol">
                              <button type="submit" className="btn btn-primary btn-block login-btn mb-3">Додати курс</button>
                              {wrongData && <p className="text-danger">Щось пішло не так!</p>}
                          </div>
                      </form>
                  </div>
              </div>
  </div>
    )
}

export default CourseCreate