import React, {useState, useEffect} from 'react'
import './course-edit.css'
import axios from 'axios'
import { useForm } from "react-hook-form"
import { Link, NavLink, useHistory, useLocation,  useParams } from 'react-router-dom'
import icon from "../../../assets/course_icon.png"
import {Accordion, Card} from  'react-bootstrap'

const CourseEdit = ({isMobile, apiUrl, user}) => {

    const [name, setName] = useState()
    const [topics, setTopics] = useState()
  
    let history = useHistory()
    let { courseId } = useParams()
    let location = useLocation()

    useEffect(() => {
        let mounted = true

        axios.get(`${apiUrl}/get_course?course_id=${courseId}`)
        .then(res => {
            if (mounted){
                setName(res.data.name)
                setTopics(res.data.topics)
            } 
        })
        .catch(err => console.error(err))

        return () => mounted = false
    }, [courseId])


    const courseTopics = topics && topics.map((el, index) => {

        return <Card key={index}>
              <Card.Header >
                <div className="d-flex flex-row justify-content-between">
                    <div className="align-self-center">
                       Тема {index+1}. {el.title}
                    </div>
                    <div className="d-flex flex-column" >
                    <Link to={{pathname: `/courses/${courseId}/topic/${el.topic_id}`, state: location.state}}> 
                        <span>Переглянути</span>
                    </Link>
                    <Link to={{pathname: `/courses/${courseId}/topic/${el.topic_id}/edit`, state: location.state}}> 
                        <span>Редагувати</span>
                    </Link>
                </div>
                </div>
            </Card.Header>
    </Card>
      })
  


    if (!topics){
        return (
            <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
            <h5 >
                Завантаження...
            </h5>
        </div>
        )
    }

    return (
        <div className ="container my-3  coursePage">
              <div className="col mx-auto mt-5 text-center">
                  <h1 className="my-5">{name}</h1>
              </div>
              <div className="col-md-3 mx-auto">
                  <img className="course-img" src={icon} alt='courseIcon' />
              </div>
              <div className="col mx-auto my-3"> 
              <h4>Теми курсу</h4> 
                <Accordion className="mt-4  edit-course">
                    {courseTopics}
                    <Card>

                    <Link to={{pathname: `/courses/${courseId}/create_topic`, state: location.state}}> 
                        <Card.Header>
                            + Додати Тему
                        </Card.Header>
                    </Link>
                    </Card>
                </Accordion>
              </div>      
         </div>
    )
}

export default CourseEdit