import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { useParams, useLocation, Link} from 'react-router-dom';
import icon from "../../../assets/course_icon.png"
import plug from "../../../assets/course_plug.png"
import teacherIcon from "../../../assets/teacher-icon.png"
import './course.css'

const Course = ({isMobile, apiUrl, user}) => {

    const [name, setName] = useState()
    const [enrolled, setEnrolled] = useState(true)
    const [unenrolled, setUnenrolled] = useState(true)
    const [description, setDescription] = useState()
    const [students, setStudents] = useState()

    let location = useLocation()
    let { courseId } = useParams()


    useEffect(() => {
        let mounted = true

        axios.get(`${apiUrl}/get_course?course_id=${courseId}`)
          .then(res => {
             if (mounted){
               setName(res.data.name)
               setDescription(res.data.description)
               setStudents(res.data.enrolledStudents)
             } 
          })
          .catch(err => console.error(err))
    
        return () => mounted = false
    }, [courseId])


    useEffect(() => {
    
      if (students && location.state){
        const fullName = `${location.state.name} ${location.state.surname}`
        const studentNames = students.map(el => el.full_name)
        const enroll = studentNames.includes(fullName)
        setEnrolled(enroll)
        setUnenrolled(!enroll)
      }
  }, [students, location.state])


    const enrollStudentHandler = () => {
  
      const data =JSON.stringify({
        "course_id":  parseInt(courseId)
      })

      axios.post(`${apiUrl}/enroll`,
         data,
         {
          headers: {
                'content-type': 'application/json',
                 'Authorization': `Basic ${location.state.sessionId}`
          }
        }).then(() => { 
            window.location.reload()
        })
        .catch(err => {
            console.log('Error on enrolling student', err);
        })

    }

    const unenrollStudentHandler = () => {

      const data =JSON.stringify({
        "course_id":  parseInt(courseId)
      })
  
      axios.post(`${apiUrl}/unenroll`, 
        data,
        {
        headers: {
              'Content-Type': 'application/json',
               'Authorization': `Basic ${location.state.sessionId}`
        }
      }).then(() => { 
          window.location.reload()
      })
        .catch(err => {
            console.log('Error on unnrolling student', err);
        })
    }

  

    if (!name){
      return (
          <div role="main" className="container courses maincontainer mt-4 mb-4 mt-md-5 mb-md-5">
          <h5 >
              Завантаження...
          </h5>
      </div>
      )
  }
    

    return (
        <div className ="container my-3  coursePage">

           <div className="row my-md-5 my-3">          
                    <div className="col-md-6 d-flex flex-column">
                        <h1 className="my-3">{name }</h1>
                        <p>{description}</p>

                     {location.state && location.state.role === 'student' &&     
                        <>
                         {unenrolled && <button className="btn main-btn  btn-primary btn-lg mt-auto mb-1" onClick={enrollStudentHandler}  disabled={enrolled}>Записатися</button> }
                         {enrolled && <button className="btn main-btn  btn-danger btn-lg  mt-auto mb-1" onClick={unenrollStudentHandler} disabled={unenrolled}>Виписатися</button>}
                        </>
                    }


                   {location.state && location.state.role === 'teacher' &&     
                      
                      <Link className="register-link" to={{pathname: `/courses/${courseId}/edit`, state: location.state}}> <button className="btn main-btn  btn-primary btn-lg mt-auto mb-1" >Редагувати</button> </Link>
                       
                    }
                    </div>
                    {!isMobile&&
                      <div className="col-md-3">
                        <img className="course-img" src={icon} alt='courseIcon' />
                    </div>
                    } 
                    <div className="col-12 pt-3">
                    </div>
            </div>

            <div className="row justify-content-center mb-4">  
                         
                    <div className="col-md-10">
                      <h2 className="mt-3 mb-4 text-center">Чому ми робимо цей курс?</h2>
                     <p className="text-center info">
                        Світова криза, спричинена пандемією, змінила економічне середовище і правила гри на ринку. Практично всі бізнеси втратили надходження і змушені переглядати або шукати нові бізнес-моделі. Цей курс дає інструменти як це зробити. Після завершення навчання власник розумітиме, які знання та дані необхідні для того, щоб бізнес був успішним.
                     </p>
                    </div>
      
           </div>

            <div className="row ">  
                   {!isMobile&&
                      <div className="col-md-4 my-auto  p-5">
                        <img className="course-img" src={plug} alt='coursePlug' />
                    </div>
                    }         
                    <div className="col-md-8">
                    <h2 className="mt-3 mb-4">На курсі ви з’ясуєте:</h2>
                    <p>
                      •	хто є вашими клієнтами, а хто — ні? Як знайти «своїх» клієнтів?<br/><br/>
                      •	у чому ваша унікальна ціннісна пропозиція? Що саме ви пропонуєте своїм клієнтам, і чому вони купують у вас?<br/><br/>
                      •	як виглядає ваша модель прибутку та що має бути в стратегічному фокусі бізнесу?<br/><br/>
                      •	як вашому бізнесу не опинитись у галузі, з якої мігрує капітал?<br/><br/>
                      •	що саме належить до стратегічного контролю вашої компанії? Як ви захищаєте свій прибуток від конкурентів?<br/><br/>
                      •	які інструменти допоможуть розглянути власну бізнес-модель, визначити та вдосконалити її ефективність?
                     </p>
                    </div>
                    <div className="col-12 pt-3">
                    </div>
           </div>
           <h2 className="mt-3 mb-4">Наші спікери</h2>
           <div className="row justify-content-around mt-3">  
                    <div className="col-md-3 col-lg-2">
                        <img className={`${!isMobile ? 'teacher-img' : 'teacher-img-mob mx-auto d-block'}`} src={teacherIcon} alt='teacherIcon' />
                    </div>
                    <div className="col-md-7  col-lg-8 teacher-info">
                       <h3 className="my-3">Олександр Соколовський</h3>
                       <h5 className="my-3">Керівник відділу</h5>
                       <p className="my-3">Історія про нього......</p>
                    </div>           
           </div>
           <div className="row justify-content-around mt-3">  
                    <div className="col-md-3 col-lg-2">
                        <img className={`${!isMobile ? 'teacher-img' : 'teacher-img-mob mx-auto d-block'}`} src={teacherIcon} alt='teacherIcon' />
                    </div>
                    <div className="col-md-7  col-lg-8 teacher-info">
                       <h3 className="my-3">Олександр Соколовський</h3>
                       <h5 className="my-3">Керівник відділу</h5>
                       <p className="my-3">Історія про нього......</p>
                    </div>           
           </div>
           <div className="row justify-content-around mt-3">  
                    <div className="col-md-3 col-lg-2">
                        <img className={`${!isMobile ? 'teacher-img' : 'teacher-img-mob mx-auto d-block'}`} src={teacherIcon} alt='teacherIcon' />
                    </div>
                    <div className="col-md-7  col-lg-8 teacher-info">
                       <h3 className="my-3">Олександр Соколовський</h3>
                       <h5 className="my-3">Керівник відділу</h5>
                       <p className="my-3">Історія про нього......</p>
                    </div>           
           </div>

          {/* <p>Enrolled students:</p>
          <ul>
            {enrolledStudents}
          </ul> */}
          
        </div>
    )
}

export default Course