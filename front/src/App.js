import React, { useState, useEffect, useCallback } from 'react'
import { Route, Switch, Redirect, useHistory, useRouteMatch,useParams, useLocation } from "react-router-dom"
import axios from 'axios'
import AboutUs from './components/AboutUs/about-us'
import Courses from './components/Courses/courses'
import Course from './components/Courses/Course/course'
import CourseCreate from './components/Courses/CourseCreate/course-create'
import TopicCreate from './components/Topics/TopicCreate/topic-create'
import TopicEdit from './components/Topics/TopicEdit/topic-edit'
import Topic from './components/Topics/Topic/topic'
import CourseEdit from './components/Courses/CourseEdit/course-edit'
import Header from './components/Header/header'
import Main from './components/Main/main'
import Login from './components/Login/login'
import Register from './components/Register/register'
import Page404 from './components/Page404/page404'
import Homework from './components/Info/Homeworks/homework'
import Mark from './components/Info/Marks/mark'
import Personal from './components/Info/Personal/personal'


const API_URL = 'https://blooming-fortress-96275.herokuapp.com'

const App = () => {

  const [mobile, setMobile] = useState(window.innerWidth < 768)
  const [auth, setAuth] = useState(true)
  const [user, setUser] = useState(null)

  let location = useLocation()
  let history = useHistory()



  


  useEffect(() => {

    const logOut = () => {
      localStorage.removeItem("sessionId")
      setUser(null)
      location.state = null 
    }

    setAuth(!(location.pathname === '/login' || location.pathname === '/register'))

    const id = localStorage.getItem("sessionId")

    if (id && location.state){
      user 
        ? id !== location.state.sessionId && logOut()
        : setUser(location.state)     
    }else{
      if (user)
        location.state = user
    }


 
  

     //console.log("path", location.state)
  }, [location, user])



  useEffect(() => {
    const handleResize = () => {
      setMobile(() => window.innerWidth < 768)
    }
    handleResize()
    window.addEventListener("resize", handleResize)
    return () => window.removeEventListener("resize", handleResize)
  }, [])



  // useEffect(() => {
  //  console.log("user", user)
  // }, [user])



  const handleLogin = useCallback(({ name, surname, email, role, sessionId }) => {
    setUser({
      name: name,
      surname: surname,
      email: email,
      role: role,
      sessionId: sessionId
    })
  }, [])


  const genereteSessionId = () => {
    return Date.now().toString(36) + Math.random().toString(36).substring(2)
  }


  return (
    <div className="app">
      <Header isMobile={mobile} isAuth={auth} user={user} handleUser={setUser} />
      
      <Switch>


         
        {user === null && <Redirect exact from="/homework" to='/' />}
        {user === null && <Redirect exact from="/marks" to='/' />}
        {user === null && <Redirect exact from="/personal" to='/' />}
        {user === null && <Redirect exact from="/mycourses" to='/' />}
        {user === null  && <Redirect exact from="/courses/:courseId/topic/:topicId" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/courses/:courseId/edit" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/courses/:courseId/create_topic" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/courses/:courseId/topic/:topicId/edit" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/courses/:courseId/create_topic" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/courses/:courseId/topic/:topicId/edit" to='/' />}
        {(user === null || user.state ==='student')  && <Redirect exact from="/create_course" to='/' />}
        {/* {user && user.role === 'student' ? <Redirect exact from="/homework" to={{pathname: '/', state: user}} /> : <Redirect exact from="/marks" to={{pathname: '/', state: user}} />} */}

        <Route exact path="/" render={() => <Main user={user} apiUrl={`${API_URL}/get_all_courses`}/>} />
        <Route exact path="/about" render={() => <AboutUs />} />
        <Route exact path="/courses" render={() => <Courses key={location.key} isAll={true} user={user} apiUrl={API_URL}/>} />
        <Route exact path="/courses/:courseId" render={() => <Course isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
        <Route exact path="/courses/:courseId/topic/:topicId" render={() => <Topic isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
        
        {/* {location.state && location.state.role==='teacher' &&
         <> */}
          <Route exact path="/courses/:courseId/edit" render={() => <CourseEdit isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
          <Route exact path="/courses/:courseId/create_topic" render={() => <TopicCreate isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
          <Route exact path="/courses/:courseId/topic/:topicId/edit" render={() => <TopicEdit isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
          <Route exact path="/create_course" render={() => < CourseCreate isMobile={mobile}  apiUrl={API_URL} user={user}/>}/>
        {/* </>} */}

       
        <Route exact path="/login" render={() => <Login handleEntered={handleLogin} sessionId={genereteSessionId} apiUrl={`${API_URL}/login`}/>} />
        <Route exact path="/register" render={() => <Register handleEntered={handleLogin} sessionId={genereteSessionId} apiUrl={`${API_URL}/register`}/>} />
        
        {/* {location.state &&
        <> */}
          <Route exact path="/personal" render={() => <Personal />} />
          <Route exact path="/homework" render={() => <Homework apiUrl={API_URL} />} />
          <Route exact path="/marks" render={() => <Mark apiUrl={API_URL} />} />
          <Route exact path="/mycourses" render={() => <Courses key={location.key} isAll={false} user={user} apiUrl={API_URL}/>} />
        {/* </>} */}
        <Route path="*" component={Page404} />

      </Switch>
    </div>
  )
}

export default App
