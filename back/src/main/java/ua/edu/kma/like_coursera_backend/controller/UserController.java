package ua.edu.kma.like_coursera_backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ua.edu.kma.like_coursera_backend.entity.User;
import ua.edu.kma.like_coursera_backend.repository.UserRepository;

import java.util.Base64;
import java.util.Map;
import java.util.Optional;

@RestController
public class UserController {
    @Autowired
    UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @PostMapping("/")
    public ResponseEntity<String> initial() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();

        return ResponseEntity.ok("login successful " + email);
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody Map<String, Object> body) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        String username = (String) body.get("username");
        String password = (String) body.get("password");
        String full_name = (String) body.get("full_name");
        String role = (String) body.get("role");

        if (userRepository.existsUserByUsername(username)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Email already taken!");
        }
        String pass = encoder.encode(password);
        User user = new User(username, pass, full_name, role);
        log.info("Registering " + userRepository.save(user));
        log.info("created user: " + user);

        return new ResponseEntity<String>((
                String.format("{\"token\": \"%s\", \"fullName\":\"%s\", \"role\": \"%s\"}",
                        Base64.getEncoder().encodeToString((username+":"+password).getBytes()),
                        user.getFull_name(), user.getRole())

        ), HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody Map<String, Object> body){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String username = (String) body.get("username");
        String password = (String) body.get("password");
        Optional<User> user = userRepository.findByUsername(username);
        if(user.isPresent() && encoder.matches(password, user.get().getPassword()))
            return new ResponseEntity<String>((
                    String.format("{\"token\": \"%s\", \"fullName\":\"%s\", \"role\": \"%s\"}",
                            Base64.getEncoder().encodeToString((username+":"+password).getBytes()),
                            user.get().getFull_name(), user.get().getRole())

            ), HttpStatus.OK);
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No such username and/or password");
    }

    @GetMapping("/get_user_info")
    public User getUserInfo(@RequestParam(value = "user_id") long userid){
        return userRepository.findById(userid).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No such user"));
    }


}
