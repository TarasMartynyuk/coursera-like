package ua.edu.kma.like_coursera_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.kma.like_coursera_backend.entity.Course;
import ua.edu.kma.like_coursera_backend.entity.Topic;

import java.util.List;
import java.util.Optional;

public interface TopicRepository extends JpaRepository<Topic, Long> {
    List<Topic> findAllByCourseId(Long courseId);
}