package ua.edu.kma.like_coursera_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.kma.like_coursera_backend.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    boolean existsUserByUsername(String username);
}
