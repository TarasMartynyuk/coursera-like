package ua.edu.kma.like_coursera_backend.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ua.edu.kma.like_coursera_backend.entity.*;
import ua.edu.kma.like_coursera_backend.repository.*;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class CourseController {
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    TopicRepository topicRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    HomeworkRepository homeworkRepository;
    @Autowired
    StudentHomeworkRepository studentHomeworkRepository;

    private final Logger log = LoggerFactory.getLogger(CourseController.class);

    @PostMapping("/create_course")
    public long createCourse(@RequestBody Map<String, Object> body) {
        System.out.println("Hello, i am here");
        Course course = new Course();
        course.setName((String) body.get("name"));
        course.setDescription((String) body.getOrDefault("description", "Description not set"));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(authentication.getName()).get();
        course.setOwnerId(user.getId());

        Course createdCourse = courseRepository.save(course);
        log.info("created course: " + course);

        return createdCourse.getId();
    }

    @PostMapping("/add_topic")
    @Transactional
    public ResponseEntity<String> addTopic(@RequestBody Map<String, Object> body) throws ParseException {

        long courseId = (Integer) body.get("course_id");
        if (!isCourseOwner(courseId)) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("current user is not an owner of this course");
        }

        Topic topic = new Topic();
        topic.setTitle((String) body.get("title"));
        topic.setBody((String) body.get("body"));
        topic.setCourseId(courseId);

        topicRepository.save(topic);
        Homework homework = Homework.builder()
                .topic(topic)
                .description((String)body.get("homework_description"))
                .deadline(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse((String)body.get("homework_deadline")))
                .build();

        topic.setHomework(homework);

        homeworkRepository.save(homework);


        Set<User> students =  courseRepository.getOne(courseId).getEnrolledStudents();

        List<StudentHomework> studentHomeworks = new ArrayList<>();
        for(User student: students){
            studentHomeworks.add(StudentHomework.builder().student(student).rootHomework(homework).build());
        }

        studentHomeworkRepository.saveAll(studentHomeworks);

        return ResponseEntity.ok(String.valueOf(topic.getId()));
    }

    @PostMapping("/edit_topic")
    public ResponseEntity<String> editTopic(@RequestBody Map<String, Object> body) throws ParseException {

        long topicId = (Integer) body.get("topic_id");
        Topic topic = topicRepository.getOne(topicId);


        Homework homework = topic.getHomework();

        if (!isCourseOwner(topic.getCourseId())) {
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("current user is not an owner of this course");
        }

        topic.setTitle((String) body.get("title"));
        topic.setBody((String) body.get("body"));
        topicRepository.save(topic);

        homework.setDescription((String)body.get("homework_description"));
        homework.setDeadline((new SimpleDateFormat("yyyy-MM-dd HH:mm").parse((String)body.get("homework_deadline"))));
        homeworkRepository.save(homework);

        return ResponseEntity.ok("");
    }

    @DeleteMapping("/delete_topic")
    public ResponseEntity<String> deleteTopic(@RequestParam(value = "topic_id") long topicId) {
        topicRepository.deleteById(topicId);
        return ResponseEntity.ok(String.valueOf(topicId));
    }

    @GetMapping("/get_all_courses")
    public List<Course> getAllCourses() throws JsonProcessingException {
        return courseRepository.findAll();
    }

    @GetMapping("/get_course")
    public ResponseEntity<Object> getCourse(@RequestParam(value = "course_id") long courseId) throws JsonProcessingException {
        Course course = courseRepository.findById(courseId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No course with id "+courseId));
        List<Topic> topics = topicRepository.findAllByCourseId(courseId);
        Map<String, Object> map = new HashMap<>();
        map.put("id", course.getId());
        map.put("name", course.getName());
        map.put("description", course.getDescription());
        map.put("ownerId", course.getOwnerId());
        map.put("enrolledStudents", course.getEnrolledStudents());
        List<Map<String, Object>> mapList = new ArrayList<>();
        for(Topic topic: topics){
            Map<String, Object> temp = new HashMap<>();
            temp.put("topic_id", topic.getId());
            temp.put("title", topic.getTitle());
            temp.put("body", topic.getBody());
            temp.put("homework_id", topic.getHomework().getId());
            temp.put("homework_description", topic.getHomework().getDescription());
            temp.put("homework_deadline", topic.getHomework().getDeadline());
            mapList.add(temp);
        }
        map.put("topics", mapList);

        return new ResponseEntity<Object>(map, HttpStatus.OK);
    }

    boolean isCourseOwner(long courseId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(authentication.getName()).get();
        Course course = courseRepository.getOne(courseId);
        return course.getOwnerId() == user.getId();
    }

}
