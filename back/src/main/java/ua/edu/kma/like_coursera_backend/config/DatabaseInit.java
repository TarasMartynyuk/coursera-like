package ua.edu.kma.like_coursera_backend.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ua.edu.kma.like_coursera_backend.entity.User;
import ua.edu.kma.like_coursera_backend.repository.CourseRepository;
import ua.edu.kma.like_coursera_backend.repository.UserRepository;

@Configuration
class DatabaseInit {

    private static final Logger log = LoggerFactory.getLogger(DatabaseInit.class);

    @Bean
    CommandLineRunner initDatabase(UserRepository repository) {
        return args -> {
            if (repository.existsUserByUsername("teacher1@gmail.com")) {
                return;
            }
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String pass = encoder.encode("1111111");
            log.info("  Preloading " + repository.save(new User("teacher1@gmail.com", pass, "Ivan Kornienko", "teacher")));
            log.info("Preloading " + repository.save(new User("student1@gmail.com", pass, "Vasiliy Ivanov", "student")));
        };
    }

}