package ua.edu.kma.like_coursera_backend.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "topic")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "body", nullable = false)
    private String body;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name = "homework_id", referencedColumnName = "id", nullable = true)
    private Homework homework;

    @Column(name = "course_id", nullable = false)
    private long courseId;
}
