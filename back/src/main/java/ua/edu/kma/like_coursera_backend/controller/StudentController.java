package ua.edu.kma.like_coursera_backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ua.edu.kma.like_coursera_backend.entity.Course;
import ua.edu.kma.like_coursera_backend.entity.StudentHomework;
import ua.edu.kma.like_coursera_backend.entity.Topic;
import ua.edu.kma.like_coursera_backend.entity.User;
import ua.edu.kma.like_coursera_backend.repository.CourseRepository;
import ua.edu.kma.like_coursera_backend.repository.StudentHomeworkRepository;
import ua.edu.kma.like_coursera_backend.repository.TopicRepository;
import ua.edu.kma.like_coursera_backend.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class StudentController {
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    StudentHomeworkRepository studentHomeworkRepository;

    @PersistenceContext
    EntityManager entityManager;

    private final Logger log = LoggerFactory.getLogger(StudentController.class);

    @GetMapping("/get_enrolled_in_courses")
    @Transactional
    public List<Course> getEnrolledInCourses() {
        log.info("ENT MANAGER IS :" + entityManager);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = userRepository.findByUsername(authentication.getName());
        if(!user.isPresent())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Please login to see this page");
        
        Query query = entityManager.createNativeQuery(
                "SELECT course.id, course.name, course.description from \n" +
                        "\tcourse_enrolled_students INNER JOIN course\n" +
                        "      on  course_enrolled_students.course_id = course.id\n" +
                        "\t    where enrolled_students_id="+user.get().getId());
        List<Object[]> results = query.getResultList();

        ArrayList<Course> courses = new ArrayList<>(results.size());
        for (Object[] columns : results) {
            Course course = new Course();
            course.setId(((BigInteger) columns[0]).longValue());
            course.setName((String) columns[1]);
            course.setDescription((String) columns[2]);
            courses.add(course);
        }

        return courses;
    }


    @PostMapping("/enroll")
    public ResponseEntity<String> enroll(@RequestBody Map<String, Object> body) {
        long courseId = (Integer) body.get("course_id");
        Optional<Course> course = courseRepository.findById(courseId);
        if (!course.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("invalid course_id");
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(authentication.getName()).get();

        course.get().getEnrolledStudents().add(user);
        courseRepository.save(course.get());

        List<Topic> topics = topicRepository.findAllByCourseId(courseId);
        for(Topic topic: topics){
            studentHomeworkRepository.save(StudentHomework.builder().answer("").rootHomework(topic.getHomework()).student(user).build());
        }

        return ResponseEntity.ok("");
    }

    @PostMapping("/unenroll")
    @Transactional
    public ResponseEntity<String> unenroll(@RequestBody Map<String, Object> body) {
        long courseId = (Integer) body.get("course_id");
        Course course = courseRepository.findById(courseId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Course not found"));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(authentication.getName()).get();

        if (!course.getEnrolledStudents().contains(user)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("not enrolled");
        }
        course.getEnrolledStudents().remove(user);
        courseRepository.save(course);

        studentHomeworkRepository.deleteAllByStudent(user);

        return ResponseEntity.ok("");
    }
    
    @GetMapping("/get_homework")
    public ResponseEntity<Object> getHomework(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = userRepository.findByUsername(authentication.getName());
        if(!user.isPresent())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Please login to see this page");
        Map<String, Object> map = new HashMap<>();

        List<Map<String, Object>> list = new ArrayList<>();
        for(StudentHomework studentHomework: user.get().getStudentHomeworks()){
            list.add(Stream.of(new Object[][] {
                    {"id", studentHomework.getId()},
                    {"homework_id", studentHomework.getRootHomework().getId()},
                    {"answer", studentHomework.getAnswer()},
                    {"mark", studentHomework.getMark()}
            }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])));
        }
        map.put("homeworks", list);
        return new ResponseEntity<Object>(map, HttpStatus.OK);
    }

    @PostMapping("/assign_homework")
    public ResponseEntity<String> assignHomework(@RequestBody Map<String, Object> body){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = userRepository.findByUsername(authentication.getName());
        if (!user.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Please login to do this");
        }

        StudentHomework studentHomework = studentHomeworkRepository
                .findByRootHomework_Id(Long.parseLong((String) body.get("homework_id")))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN, "Please login to see this page"));

        studentHomework.setAnswer((String) body.get("answer"));
        studentHomeworkRepository.save(studentHomework);
        return ResponseEntity.ok("Answer saved");
    }
}
