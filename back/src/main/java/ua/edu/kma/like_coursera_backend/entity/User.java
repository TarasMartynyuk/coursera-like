package ua.edu.kma.like_coursera_backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "full_name", nullable = false)
    private String full_name;

    @Column(name = "role", nullable = false)
    private String role;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
    private Set<StudentHomework> studentHomeworks;

    @ManyToMany()
//    @JoinTable(
//            name = "course_enrolled_students",
//            joinColumns = @JoinColumn(name = "enrolled_students_id"),
//            inverseJoinColumns = @JoinColumn(name = "course_id"))
    Set<Course> enrolledCourses;

    public User(String username, String password, String full_name, String role)  {
        this();
        this.username = username;
        this.password = password;
        this.full_name = full_name;
        this.role = role;
    }
}
