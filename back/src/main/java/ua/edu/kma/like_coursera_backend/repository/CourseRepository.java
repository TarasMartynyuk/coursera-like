package ua.edu.kma.like_coursera_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.kma.like_coursera_backend.entity.Course;
import ua.edu.kma.like_coursera_backend.entity.Topic;

import javax.persistence.EntityManager;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {


}