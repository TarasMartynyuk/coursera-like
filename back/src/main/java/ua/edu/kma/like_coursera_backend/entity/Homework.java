package ua.edu.kma.like_coursera_backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "homework")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Homework {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToOne(mappedBy = "homework")
    private Topic topic;

    @Column(name = "deadline", nullable = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date deadline;

    @JsonManagedReference
    @JsonIgnore
    @Transient
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "rootHomework")
    private Set<StudentHomework> studentHomeworks;
}
