package ua.edu.kma.like_coursera_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.kma.like_coursera_backend.entity.Homework;

public interface HomeworkRepository extends JpaRepository<Homework, Long> {
}
