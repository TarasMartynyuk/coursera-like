package ua.edu.kma.like_coursera_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class LikeCourseraBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(LikeCourseraBackendApplication.class, args);
    }

}
