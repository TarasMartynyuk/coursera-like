package ua.edu.kma.like_coursera_backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.kma.like_coursera_backend.entity.StudentHomework;
import ua.edu.kma.like_coursera_backend.entity.User;

import java.util.List;
import java.util.Optional;

public interface StudentHomeworkRepository extends JpaRepository<StudentHomework, Long> {
    List<StudentHomework> deleteAllByStudent(User user);
    Optional<StudentHomework> findByRootHomework_Id(Long homeworkId);
}
